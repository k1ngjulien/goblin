module gitlab.com/k1ngjulien/goblin

go 1.15

require (
	github.com/dustinkirkland/golang-petname v0.0.0-20191129215211-8e5a1ed0cff0
	github.com/google/uuid v1.1.2
	github.com/gorilla/mux v1.8.0
)
