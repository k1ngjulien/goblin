package main

import (
	"fmt"
	"github.com/dustinkirkland/golang-petname"
	"github.com/gorilla/mux"
	"io"
	"log"
	"math/rand"
	"net/http"
	"os"
	"time"
)

func logger(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		log.Println(r.Method, r.RequestURI)
		next(w, r)
	}
}

func main() {
	rand.Seed(time.Now().UnixNano())
	r := mux.NewRouter()

	// INDEX.HTML
	r.HandleFunc("/", logger(func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "index.html")
	})).Methods("GET")

	// UPLOAD FILE
	r.HandleFunc("/", logger(func(w http.ResponseWriter, r *http.Request) {
		err := r.ParseMultipartForm(10 << 20)

		if err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		formFile, _, err := r.FormFile("file")

		id := petname.Generate(3, "-")
		file, err := os.Create(fmt.Sprintf("pastes/%v.txt", id))

		if err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		defer file.Close()

		io.Copy(file, formFile) // write file to disk

		http.Redirect(w, r, fmt.Sprintf("/pastes/%v", id), http.StatusSeeOther)
	})).Methods("POST")

	// HOST UPLOADED FILES
	r.HandleFunc("/pastes/{paste_id}", logger(func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)

		http.ServeFile(w, r, fmt.Sprintf("pastes/%v.txt", vars["paste_id"]))
	})).Methods("GET")

	log.Println("Starting Server...")
	log.Fatal(http.ListenAndServe(":8080", r))
}
