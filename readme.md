# GOBLIN

Minimal pastebin-like server written in Go.

Allows you to upload files and share a randomly generated link to them

Fileupload works through the browser or over curl.

Curl:

```
$ curl -v -F file=@<filename> localhos:8080
...
< Location: /pastes/<some unique name>
...
```
